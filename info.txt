## name ##

ob_DeciderMusebot

## version ##

1.0

## instrumentation ##

type:
    @textures

## platform ##

java

## behaviour ##

crazy Ollie stuff

## messages ##

sends:
    @unknown

receives:
    @unknown

## license ##

Creative Commons Share Alike

## credits ##

ollie bown