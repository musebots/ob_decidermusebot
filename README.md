# Ollie Bown's DeciderMusebot #

[Mac executable and audio samples](https://www.sfu.ca/musebots/Musebot_Test_Suite/Musebots/Noise_Texture_Non-beat_generators/ob_DeciderMusebot.zip)

## version ##

1.0

## instrumentation ##

type:

      @textures

## platform ##

java

## behaviour ##

crazy Ollie stuff

## messages ##

sends:

      @unknown

receives:

      @unknown

## license ##

Creative Commons Share Alike

## credits ##

ollie bown
